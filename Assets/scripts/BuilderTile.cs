﻿using UnityEngine;
using System.Collections;

public class BuilderTile : MonoBehaviour
{
    public int X { get; private set; }
    public int Y { get; private set; }
    TileType tileType;
    public bool IsPathTile { get; private set; }

    public void Setup(int x, int y, bool isPathTile)
    {
        this.X = x;
        this.Y = y;
        this.IsPathTile = isPathTile;
        this.tileType = (isPathTile) ? TileType.Path_Black : TileType.Border_Empty;
    }

    public void Setup(int x, int y, bool isPathTile, TileType type)
    {
        this.X = x;
        this.Y = y;
        this.IsPathTile = isPathTile;
        this.tileType = type;
    }

    public TileType HandleButtonClick()
    {
        switch (tileType)
        {
            case TileType.Path_Black:
                tileType = TileType.Path_White; break;
            case TileType.Path_White:
                tileType = TileType.Path_Black; break;

            case TileType.Path_Black_Start:
                tileType = TileType.Path_White_Start; break;
            case TileType.Path_White_Start:
                tileType = TileType.Path_Black_Start; break;

            case TileType.Border_Empty:
                tileType = TileType.Border_NE; break;
            case TileType.Border_NE:
                tileType = TileType.Border_NS; break;
            case TileType.Border_NS:
                tileType = TileType.Border_NSE; break;
            case TileType.Border_NSE:
                tileType = TileType.Border_NW; break;
            case TileType.Border_NW:
                tileType = TileType.Border_NWE; break;
            case TileType.Border_NWE:
                tileType = TileType.Border_NWS; break;
            case TileType.Border_NWS:
                tileType = TileType.Border_NWSE; break;
            case TileType.Border_NWSE:
                tileType = TileType.Border_SE; break;
            case TileType.Border_SE:
                tileType = TileType.Border_WE; break;
            case TileType.Border_WE:
                tileType = TileType.Border_WS; break;
            case TileType.Border_WS:
                tileType = TileType.Border_WSE; break;
            case TileType.Border_WSE:
                tileType = TileType.Border_Empty; break;
            default:
                break;
        }
        return tileType;
    }
}

public enum TileType
{
    NULL,
    Path_Black,
    Path_White,
    Path_Black_Start,
    Path_White_Start,
    Border_Empty,
    Border_NE,
    Border_NS,
    Border_NSE,
    Border_NW,
    Border_NWE,
    Border_NWS,
    Border_NWSE,
    Border_SE,
    Border_WE,
    Border_WS,
    Border_WSE
}