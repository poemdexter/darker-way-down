﻿using System.Collections.Generic;
using MiniJSON;
using System;
using UnityEngine;

public class Puzzle {

    public string Title { get; private set; }
    public int Size { get; private set; }
    public TileType[,] WalkGrid { get; private set; }
    public TileType[,] BorderGrid { get; private set; }

    public Puzzle(int size, string title)
    {
        WalkGrid = new TileType[2 * size + 1, 2 * size + 1];
        BorderGrid = new TileType[2 * size + 1, 2 * size + 1];
        Title = title;
        Size = size;
    }

    public Puzzle(string title, int size, List<object> walkGrid, List<object> borderGrid)
    {
        Title = title;
        Size = size;
        WalkGrid = new TileType[2 * size + 1, 2 * size + 1];
        BorderGrid = new TileType[2 * size + 1, 2 * size + 1];

        int i = 0;
        for (int j = 0; j < walkGrid.Count; j++)
        {
            if (j > 0 && j % (2 * size + 1) == 0) i++;
            WalkGrid[i, j % (2 * size + 1)] = (TileType)Enum.Parse(typeof(TileType), (string)walkGrid[j]);
        }

        int k = 0;
        for (int l = 0; l < borderGrid.Count; l++)
        {
            if (l > 0 && l % (2 * size + 1) == 0) k++;
            BorderGrid[k, l % (2 * size + 1)] = (TileType)Enum.Parse(typeof(TileType), (string)borderGrid[l]);
        }
    }

    public void SetPathTileType(int x, int y, TileType type)
    {
        WalkGrid[x, y] = type;
    }

    public void SetBorderTileType(int x, int y, TileType type)
    {
        BorderGrid[x, y] = type;
    }

    public string Serialize()
    {
        Dictionary<string, object> puzzle = new Dictionary<string, object>();
        puzzle.Add("Title", Title);
        puzzle.Add("Size", Size);
        puzzle.Add("WalkGrid", WalkGrid);
        puzzle.Add("BorderGrid", BorderGrid);
        return Json.Serialize(puzzle);
    }
}