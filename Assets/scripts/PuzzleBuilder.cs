﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using MiniJSON;
using System.IO;
using UnityEditor;
using System.Collections.Generic;

public class PuzzleBuilder : MonoBehaviour
{
    public GameObject tile;
    public Sprite pathBlack_Sprite, pathWhite_Sprite;
    public Sprite pathBlackStart_Sprite, pathWhiteStart_Sprite;
    public Sprite borderEmpty_Sprite, borderNE_Sprite, borderNS_Sprite, borderNSE_Sprite;
    public Sprite borderNW_Sprite, borderNWE_Sprite, borderNWS_Sprite, borderNWSE_Sprite;
    public Sprite borderSE_Sprite, borderWE_Sprite, borderWS_Sprite, borderWSE_Sprite;

    public InputField titleText;
    public Text titleLabel;

    private string puzzleTitle;
    private Puzzle puzzle;
    private float offset = 0.5f;
    private bool isSetStart = false;

    void ResetPuzzlePanel()
    {
        for (int i = transform.childCount - 1; i >= 0; i--)
        {
            GameObject.Destroy(transform.GetChild(i).gameObject);
        }
        transform.localPosition = Vector2.zero;
    }

    public void SetPuzzleTitle(Text titleText)
    {
        puzzleTitle = titleText.text;
    }

    public void LoadGrid(Puzzle puzzle)
    {
        int size = puzzle.Size;
        ResetPuzzlePanel();
        for (int x = 0; x < 2 * size + 1; x++)
        {
            for (int y = 0; y < 2 * size + 1; y++)
            {
                GameObject newBlock = Instantiate(tile) as GameObject;
                newBlock.transform.SetParent(transform, false);
                newBlock.transform.position = new Vector2(offset * x, offset * y);
                newBlock.name = x + "," + y;
                if (x % 2 != 0 && y % 2 != 0)
                {
                    // PATH TILE
                    SetTileSprite(newBlock, puzzle.WalkGrid[x, y]);
                    newBlock.GetComponent<BuilderTile>().Setup(x, y, true, puzzle.WalkGrid[x, y]);
                }
                else
                {
                    // BORDER TILE
                    SetTileSprite(newBlock, puzzle.BorderGrid[x, y]);
                    newBlock.GetComponent<BuilderTile>().Setup(x, y, false, puzzle.BorderGrid[x, y]);
                }
            }
        }
        transform.localPosition = new Vector2(100 - (37 * size), -37 * size);
    }

    public void CreateNewGrid(Text sizeText)
    {
        if (sizeText.text == "") return;
 
        int size = int.Parse(sizeText.text);
        ResetPuzzlePanel();
        puzzle = new Puzzle(size, puzzleTitle);

        for (int x = 0; x < 2 * size + 1; x++)
        {
            for (int y = 0; y < 2 * size + 1; y++)
            {
                GameObject newBlock = Instantiate(tile) as GameObject;
                newBlock.transform.SetParent(transform, false);
                newBlock.transform.position = new Vector2(offset * x, offset * y);
                newBlock.name = x + "," + y;
                if (x % 2 != 0 && y % 2 != 0)
                {
                    // PATH TILE
                    newBlock.GetComponent<Image>().sprite = pathBlack_Sprite;
                    newBlock.GetComponent<BuilderTile>().Setup(x, y, true);
                    puzzle.SetPathTileType(x, y, TileType.Path_Black);
                    SetTileSprite(newBlock, TileType.Path_Black);
                }
                else
                {
                    // BORDER TILE
                    newBlock.GetComponent<Image>().sprite = borderEmpty_Sprite;
                    newBlock.GetComponent<BuilderTile>().Setup(x, y, false);
                    puzzle.SetBorderTileType(x, y, TileType.Border_Empty);
                    SetTileSprite(newBlock, TileType.Border_Empty);
                }
            }
        }

        transform.localPosition = new Vector2(100 - (37 * size), -37 * size);
        SavePuzzle();
    }

    public void HandleSetStartGUIClick()
    {
        GameObject.FindGameObjectWithTag("PuzzlePanel").GetComponent<PuzzleBuilder>().HandleSetStartClick();
    }

    public void HandleSetStartClick()
    {
        isSetStart = true;
    }

    public void HandlePuzzleGUIClick(RectTransform t)
    {
        GameObject.FindGameObjectWithTag("PuzzlePanel").GetComponent<PuzzleBuilder>().HandlePuzzleTileClick(t);
    }

    public void HandlePuzzleTileClick(RectTransform t)
    {
        if (isSetStart)
        {
            isSetStart = false;
        }
        else
        {
            BuilderTile tile = t.gameObject.GetComponent<BuilderTile>();
            TileType newType = tile.HandleButtonClick();
            if (tile.IsPathTile)
            {
                puzzle.SetPathTileType(tile.X, tile.Y, newType);
            }
            else
            {
                puzzle.SetBorderTileType(tile.X, tile.Y, newType);
            }
            SetTileSprite(t.gameObject, newType);
        }
    }

    private void SetTileSprite(GameObject go, TileType type)
    {
        switch (type)
        {
            case TileType.Path_Black:
                go.GetComponent<Image>().sprite = pathBlack_Sprite; break;
            case TileType.Path_White:
                go.GetComponent<Image>().sprite = pathWhite_Sprite; break;
            case TileType.Path_Black_Start:
                go.GetComponent<Image>().sprite = pathBlackStart_Sprite; break;
            case TileType.Path_White_Start:
                go.GetComponent<Image>().sprite = pathWhiteStart_Sprite; break;
            case TileType.Border_Empty:
                go.GetComponent<Image>().sprite = borderEmpty_Sprite; break;
            case TileType.Border_NE:
                go.GetComponent<Image>().sprite = borderNE_Sprite; break;
            case TileType.Border_NS:
                go.GetComponent<Image>().sprite = borderNS_Sprite; break;
            case TileType.Border_NSE:
                go.GetComponent<Image>().sprite = borderNSE_Sprite; break;
            case TileType.Border_NW:
                go.GetComponent<Image>().sprite = borderNW_Sprite; break;
            case TileType.Border_NWE:
                go.GetComponent<Image>().sprite = borderNWE_Sprite; break;
            case TileType.Border_NWS:
                go.GetComponent<Image>().sprite = borderNWS_Sprite; break;
            case TileType.Border_NWSE:
                go.GetComponent<Image>().sprite = borderNWSE_Sprite; break;
            case TileType.Border_SE:
                go.GetComponent<Image>().sprite = borderSE_Sprite; break;
            case TileType.Border_WE:
                go.GetComponent<Image>().sprite = borderWE_Sprite; break;
            case TileType.Border_WS:
                go.GetComponent<Image>().sprite = borderWS_Sprite; break;
            case TileType.Border_WSE:
                go.GetComponent<Image>().sprite = borderWSE_Sprite; break;
            default:
                break;
        }
    }

    public void SavePuzzle()
    {
        string puzzleJson = puzzle.Serialize();
        File.WriteAllText("Assets/puzzles/" + titleText.text + ".json", puzzleJson);
        AssetDatabase.Refresh();
        titleLabel.text = titleText.text;
    }

    public void LoadPuzzle()
    {
        var path = EditorUtility.OpenFilePanel("Load Puzzle", "Assets/puzzles", "json");
        if (path != null && path != "")
        {
            StreamReader stream = File.OpenText(path);
            Dictionary<string, object> puzzleJson = Json.Deserialize(stream.ReadToEnd()) as Dictionary<string, object>;
            stream.Close();
            puzzle = new Puzzle(puzzleJson["Title"] as string, (int)((long)puzzleJson["Size"]), puzzleJson["WalkGrid"] as List<object>, puzzleJson["BorderGrid"] as List<object>);
            titleText.text = puzzle.Title;
            titleLabel.text = puzzle.Title;
            LoadGrid(puzzle);
        }
    }
}
